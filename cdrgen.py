import os
import datetime
import random


def gen(path):
    with open("%s.cdr" % os.path.join(path, datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')), 'w') as f:
        for i in range(random.randint(1, 1000)):
            rectype = (random.choice(('01', '02', '03', '04', '05', '06', '07', '08' '09', '10', '11', '12')), )
            f.write(','.join(rectype+tuple(map(lambda x: str(random.randint(1, 100000)), range(20))) + ('\n', )))
        f.close()



if __name__ == '__main__':
    while True:
        gen('/data00/archive/kv/lg_lon/cache')