import glob
import argparse
import os
import csv
import shutil
import json

COUNT = 'COUNT'
VOLUME = 'VOLUME'
KEY = 'key'
FILES = 'FILES'
DATE = 'DATE'
LINES = 'LINES'
BADLINES = 'BADLINES'
MARKERS = 'markers'
BADFILES_LIST_PATH = 'badfiles_list_path'
BADLINES_LIST_PATH = 'badlines_list_path'
BADFILES_FOLDER = 'badfiles_folder'
SEPARATOR = 'separator'


def getarg():
    parser = argparse.ArgumentParser()
    parser.add_argument('pattern', metavar='N', type=str, nargs='+', help='pattern')
    parser.add_argument('-o', action='store', dest='output', help='output')
    pattern = parser.parse_args().pattern[0]
    output = parser.parse_args().output
    return pattern, output


class CsvParser:
    def __init__(self, date=None, **kwargs):
        self.date = date
        self.separator = kwargs.get(SEPARATOR)
        if not self.separator:
            self.separator = ' '
        self.keys = tuple(kwargs.get(KEY, ()))
        self.markers = kwargs.get(MARKERS, {})
        self.badfiles_folder = kwargs.get(BADFILES_FOLDER)
        self.badfiles_list_path = kwargs.get(BADFILES_LIST_PATH)
        self.createhead()




    def createhead(self):
        self.head = {}
        if self.date:
            self.head[DATE] = self.date
        self.head[FILES] = 0
        self.head[LINES] = 0
        self.head.update(self.markers)


    def parse(self, file):
        try:
            f = open(file)
        except IOError:
            if self.badfiles_folder:
                shutil.copy(file, self.badfiles_folder)

            if self.badfiles_list_path:
                with open(self.badfiles_list_path, 'w') as t:
                    t.write('%s\n' % file)
                    t.close()
            return
        data = csv.reader(f, delimiter=self.separator)
        tuple(map(self.__updatehead__, data))
        self.head[FILES] += 1
        f.close()

    def __updatehead__(self, line):
        self.head[LINES] += 1
        for key in self.keys:
            head_key = key[KEY]
            if isinstance(head_key, int):
                head_key = self.getlineitem(line, head_key)
                if head_key is None:
                    return None

            if self.head.get(head_key):
                count = key.get(COUNT)
                if count:
                    self.head[head_key][COUNT] += 1
                volume = key.get(VOLUME)
                if volume:
                    volume = self.getlineitem(line, volume)
                    if volume is None:
                        return None
                    self.head[head_key][VOLUME] += int(volume)
            else:
                self.head[head_key] = {}
                count = key.get(COUNT)
                if count:
                    self.head[head_key][COUNT] = 1
                volume = key.get(VOLUME)
                if volume:
                    volume  = self.getlineitem(line, volume)
                    if volume is None:
                        return None
                    try:
                        self.head[head_key][VOLUME] = int(volume)
                    except ValueError:
                        if self.badfiles_list_path:
                            with open(self.badfiles_list_path, 'w') as t:
                                writer = csv.writer(t, self.separator)
                                writer.writerow(line)
                                t.close()
                        return None




    def getlineitem(self, line, index):
        try:
            key = line[index]
        except IndexError:
            if self.badfiles_list_path:
                with open(self.badfiles_list_path, 'w') as t:
                    writer = csv.writer(t, self.separator)
                    writer.writerow(line)
                    t.close()
            return None
        return key






if __name__ == '__main__':
    #pattern, output = getarg()
    #print(pattern)
    #errors = open('errors.list', 'w')
    #files = glob.glob(pattern)
    config = json.loads(open('config.json').read())
    parser = CsvParser(date=None, **config[0])
    parser.parse('/data00/archive/kv/lg_lon/2018-09-23/20180923064812831943.cdr')
    print(parser.head)


